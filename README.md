# push_poll

Python3 library made to create polls in LimeSurvey using data from a scraper (See Scraper_VnB)

# Usage

## Init

Feed your data to "config_data.py", then import the library in the scraper to enable it. 

## To import it :
from push_poll import PushPoll

## To call it :
PushPoll(questions,name)

## Args

- questions has to be a list containing all the questions to the poll (menu choices actually)
- name has to be a string containing the name of the restaurant which menu is scrapped.
