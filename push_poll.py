#!/usr/bin/env python
# -*- coding: utf-8 -*-


from limesurvey import Api
import base64
import config_data
from survey_templates import CreateSurvey

## Init
user = config_data.LIME_USER
key = config_data.LIME_KEY
url = config_data.LIME_API_URL
base_url = config_data.LIME_URL

## Build the API
lime = Api(url, user, key)


def PushPoll(questions, name, title):
    ## Create the survey to import, using the template for .lss files
    survey_content = CreateSurvey(questions, name)

    ## Displays the whole survey lss for debug
    #print("".join(survey_content))

    ## Join the list survey_content into a bytes string,
    ## then converts the .lss formatted survey to base64 as required by limesurvey API
    survey_b64 = base64.b64encode(bytes("".join(survey_content), 'utf-8')).decode('utf-8')

    ## Import survey in limesurvey, and get the ID from the newly created survey
    survey_id = lime.import_survey(survey_b64, title, 111111, "lss")

    ## Activate the survey
    lime.activate_survey(survey_id)

    ## Get the survey URL to send to participants
    survey_url = f'{base_url}{survey_id}'

    return(survey_url)
