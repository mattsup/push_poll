#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

def CreateSurvey(questions, name):
    survey = [f"""<?xml version="1.0" encoding="UTF-8"?>
<document>
 <LimeSurveyDocType>Survey</LimeSurveyDocType>
 <DBVersion>263</DBVersion>
 <languages>
  <language>fr</language>
 </languages>
 <groups>
  <fields>
   <fieldname>gid</fieldname>
   <fieldname>sid</fieldname>
   <fieldname>group_name</fieldname>
   <fieldname>group_order</fieldname>
   <fieldname>description</fieldname>
   <fieldname>language</fieldname>
   <fieldname>randomization_group</fieldname>
   <fieldname>grelevance</fieldname>
  </fields>
  <rows>
   <row>
    <gid><![CDATA[1]]></gid>
    <sid><![CDATA[111111]]></sid>
    <group_name><![CDATA[Le menu du {name} du {datetime.date.today().strftime("%d-%m-%Y")}]]></group_name>
    <group_order><![CDATA[1]]></group_order>
    <description/>
    <language><![CDATA[fr]]></language>
    <randomization_group/>
    <grelevance><![CDATA[1]]></grelevance>
   </row>
  </rows>
 </groups>
 <questions>
  <fields>
   <fieldname>qid</fieldname>
   <fieldname>parent_qid</fieldname>
   <fieldname>sid</fieldname>
   <fieldname>gid</fieldname>
   <fieldname>type</fieldname>
   <fieldname>title</fieldname>
   <fieldname>question</fieldname>
   <fieldname>preg</fieldname>
   <fieldname>help</fieldname>
   <fieldname>other</fieldname>
   <fieldname>mandatory</fieldname>
   <fieldname>question_order</fieldname>
   <fieldname>language</fieldname>
   <fieldname>scale_id</fieldname>
   <fieldname>same_default</fieldname>
   <fieldname>relevance</fieldname>
   <fieldname>modulename</fieldname>
  </fields>
  <rows>
    """]

    for question in questions:
        line = f"""<row>
    <qid><![CDATA[{1 + questions.index(question)}]]></qid>
    <parent_qid><![CDATA[0]]></parent_qid>
    <sid><![CDATA[111111]]></sid>
    <gid><![CDATA[1]]></gid>
    <type><![CDATA[Y]]></type>
    <title><![CDATA[Q{1 + questions.index(question)}]]></title>
    <question><![CDATA[ {str(question)} ]]></question>
    <preg/>
    <help><![CDATA[Ceci est une texte d'aide pour une question.]]></help>
    <other><![CDATA[N]]></other>
    <mandatory><![CDATA[Y]]></mandatory>
    <question_order><![CDATA[1]]></question_order>
    <language><![CDATA[fr]]></language>
    <scale_id><![CDATA[0]]></scale_id>
    <same_default><![CDATA[0]]></same_default>
    <relevance><![CDATA[1]]></relevance>
    <modulename/>
   </row> """
        survey.append(line)
    survey.append("""  </rows>
 </questions>
 <question_attributes>
  <fields>
   <fieldname>qid</fieldname>
   <fieldname>attribute</fieldname>
   <fieldname>value</fieldname>
   <fieldname>language</fieldname>
  </fields>
  <rows> 
  """)

    for question in questions:
        line = f"""   <row>
    <qid><![CDATA[{1 + questions.index(question)}]]></qid>
    <attribute><![CDATA[public_statistics]]></attribute>
    <value><![CDATA[1]]></value>
   </row> 
   """
        survey.append(line)

    survey.append("""  </rows>
 </question_attributes>
 <surveys>
  <fields>
   <fieldname>sid</fieldname>
   <fieldname>admin</fieldname>
   <fieldname>expires</fieldname>
   <fieldname>startdate</fieldname>
   <fieldname>adminemail</fieldname>
   <fieldname>anonymized</fieldname>
   <fieldname>faxto</fieldname>
   <fieldname>format</fieldname>
   <fieldname>savetimings</fieldname>
   <fieldname>template</fieldname>
   <fieldname>language</fieldname>
   <fieldname>additional_languages</fieldname>
   <fieldname>datestamp</fieldname>
   <fieldname>usecookie</fieldname>
   <fieldname>allowregister</fieldname>
   <fieldname>allowsave</fieldname>
   <fieldname>autonumber_start</fieldname>
   <fieldname>autoredirect</fieldname>
   <fieldname>allowprev</fieldname>
   <fieldname>printanswers</fieldname>
   <fieldname>ipaddr</fieldname>
   <fieldname>refurl</fieldname>
   <fieldname>publicstatistics</fieldname>
   <fieldname>publicgraphs</fieldname>
   <fieldname>listpublic</fieldname>
   <fieldname>htmlemail</fieldname>
   <fieldname>sendconfirmation</fieldname>
   <fieldname>tokenanswerspersistence</fieldname>
   <fieldname>assessments</fieldname>
   <fieldname>usecaptcha</fieldname>
   <fieldname>usetokens</fieldname>
   <fieldname>bounce_email</fieldname>
   <fieldname>attributedescriptions</fieldname>
   <fieldname>emailresponseto</fieldname>
   <fieldname>emailnotificationto</fieldname>
   <fieldname>tokenlength</fieldname>
   <fieldname>showxquestions</fieldname>
   <fieldname>showgroupinfo</fieldname>
   <fieldname>shownoanswer</fieldname>
   <fieldname>showqnumcode</fieldname>
   <fieldname>bouncetime</fieldname>
   <fieldname>bounceprocessing</fieldname>
   <fieldname>bounceaccounttype</fieldname>
   <fieldname>bounceaccounthost</fieldname>
   <fieldname>bounceaccountpass</fieldname>
   <fieldname>bounceaccountencryption</fieldname>
   <fieldname>bounceaccountuser</fieldname>
   <fieldname>showwelcome</fieldname>
   <fieldname>showprogress</fieldname>
   <fieldname>questionindex</fieldname>
   <fieldname>navigationdelay</fieldname>
   <fieldname>nokeyboard</fieldname>
   <fieldname>alloweditaftercompletion</fieldname>
   <fieldname>googleanalyticsstyle</fieldname>
   <fieldname>googleanalyticsapikey</fieldname>
  </fields>
  <rows> 
   <row>
    <sid><![CDATA[111111]]></sid>
    <admin><![CDATA[Administrator]]></admin>
    <adminemail><![CDATA[your-email@example.net]]></adminemail>
    <anonymized><![CDATA[N]]></anonymized>
    <faxto/>
    <format><![CDATA[A]]></format>
    <savetimings><![CDATA[N]]></savetimings>
    <template><![CDATA[default]]></template>
    <language><![CDATA[fr]]></language>
    <additional_languages/>
    <datestamp><![CDATA[Y]]></datestamp>
    <usecookie><![CDATA[N]]></usecookie>
    <allowregister><![CDATA[N]]></allowregister>
    <allowsave><![CDATA[Y]]></allowsave>
    <autonumber_start><![CDATA[5]]></autonumber_start>
    <autoredirect><![CDATA[Y]]></autoredirect>
    <allowprev><![CDATA[Y]]></allowprev>
    <printanswers><![CDATA[Y]]></printanswers>
    <ipaddr><![CDATA[Y]]></ipaddr>
    <refurl><![CDATA[Y]]></refurl>
    <publicstatistics><![CDATA[Y]]></publicstatistics>
    <publicgraphs><![CDATA[Y]]></publicgraphs>
    <listpublic><![CDATA[Y]]></listpublic>
    <htmlemail><![CDATA[Y]]></htmlemail>
    <sendconfirmation><![CDATA[Y]]></sendconfirmation>
    <tokenanswerspersistence><![CDATA[N]]></tokenanswerspersistence>
    <assessments><![CDATA[N]]></assessments>
    <usecaptcha><![CDATA[N]]></usecaptcha>
    <usetokens><![CDATA[N]]></usetokens>
    <bounce_email><![CDATA[your-email@example.net]]></bounce_email>
    <emailresponseto/>
    <emailnotificationto/>
    <tokenlength><![CDATA[15]]></tokenlength>
    <showxquestions><![CDATA[N]]></showxquestions>
    <showgroupinfo><![CDATA[B]]></showgroupinfo>
    <shownoanswer><![CDATA[N]]></shownoanswer>
    <showqnumcode><![CDATA[X]]></showqnumcode>
    <bounceprocessing><![CDATA[N]]></bounceprocessing>
    <showwelcome><![CDATA[N]]></showwelcome>
    <showprogress><![CDATA[N]]></showprogress>
    <questionindex><![CDATA[0]]></questionindex>
    <navigationdelay><![CDATA[0]]></navigationdelay>
    <nokeyboard><![CDATA[N]]></nokeyboard>
    <alloweditaftercompletion><![CDATA[Y]]></alloweditaftercompletion>
    <googleanalyticsstyle><![CDATA[0]]></googleanalyticsstyle>
    <googleanalyticsapikey/>
   </row>
  </rows>
 </surveys>
 <surveys_languagesettings>
  <fields>
   <fieldname>surveyls_survey_id</fieldname>
   <fieldname>surveyls_language</fieldname>
   <fieldname>surveyls_title</fieldname>
   <fieldname>surveyls_description</fieldname>
   <fieldname>surveyls_welcometext</fieldname>
   <fieldname>surveyls_endtext</fieldname>
   <fieldname>surveyls_url</fieldname>
   <fieldname>surveyls_urldescription</fieldname>
   <fieldname>surveyls_dateformat</fieldname>
   <fieldname>surveyls_attributecaptions</fieldname>
   <fieldname>surveyls_numberformat</fieldname>
   <fieldname>attachments</fieldname>
  </fields>
  <rows>
    """)

    survey.append(f"""    <row>
    <surveyls_survey_id><![CDATA[111111]]></surveyls_survey_id>
    <surveyls_language><![CDATA[fr]]></surveyls_language>
    <surveyls_title><![CDATA[Menu {name}]]></surveyls_title>
    <surveyls_description><![CDATA[Le menu du {name} du {datetime.date.today().strftime("%d-%m-%Y")}]]></surveyls_description>
    <surveyls_welcometext/>
    <surveyls_endtext/>
    <surveyls_url/>
    <surveyls_urldescription/>
    <surveyls_dateformat><![CDATA[5]]></surveyls_dateformat>
    <surveyls_numberformat><![CDATA[1]]></surveyls_numberformat>
   </row>
  </rows>
 </surveys_languagesettings>
</document>""")
    return(survey)

